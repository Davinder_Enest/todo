import  React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'react-native';
import colors from './src/consts/colors/colors';
import { RootRoutes } from './src/routes/RouteNavigator';

const App = () => {
  return (
    <NavigationContainer>
      <StatusBar backgroundColor={colors.WHITE} barStyle="dark-content" />
      <RootRoutes />
    </NavigationContainer>
  );
}

export default App;