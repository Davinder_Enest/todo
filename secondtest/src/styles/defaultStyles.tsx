import { StyleSheet } from 'react-native';
import colors from '../consts/colors/colors'

const defaultStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.WHITE,
    },
    containerJustify: {
        justifyContent: 'center'
    },
    logoView: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '8%'
    },
    logoImage: {
        height: 160,
        width: 160,
    },
    headingView: {
        margin: 10,
        marginTop:0
    },
    text_heading: {
        color: colors.BLACK,
        fontSize: 18,
        justifyContent: 'flex-start',
        marginLeft: 7,
        marginTop: -4
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        marginLeft: 5
    },
    textInput: {
        width: '95%',
        borderColor: colors.GREY,
        backgroundColor: '#FAFAFA',
        borderWidth: 1,
        margin: 5,
        height: 40,
        borderRadius: 3,
        color: colors.DARKGREY,
    },
    error:{
        marginLeft: 5,
        marginRight:5,
        width: '95%',
        color : colors.RED,
    },
    buttonsView: {
        alignItems: 'center',
        marginTop: '6%'
    },
    nonActiveButton: {
        width: '60%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: colors.WHITE,
        borderColor: colors.GREEN,
        borderWidth: 2
    },
    activebuttonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.WHITE,
    },
    nonActiveButtonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.GREEN,
    },
    activeButton: {
        width: '60%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: colors.GREEN
    },
    containerInner: {
        backgroundColor: colors.GREEN,
        flexDirection: 'row',
        justifyContent: "space-between",
        height: 55,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 20,
        marginLeft: 10,
        color: colors.WHITE,
        fontWeight: 'bold'
    },
    headerButton: {
        marginRight: 10
    },
})

export default defaultStyles;