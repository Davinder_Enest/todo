import React from 'react';
import { View, TextInput, Text, Image, ScrollView, TouchableOpacity, Alert, } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import auth from '@react-native-firebase/auth';
import colors from '../../consts/colors/colors';
import { StackNavigationProp } from '@react-navigation/stack';
import { AllRoutesName } from '../../routes/RouteNavigator';
import defaultStyles from '../../styles/defaultStyles';
import { observer } from 'mobx-react';
import { action, observable } from 'mobx';

type ScreenNavigationProp = StackNavigationProp<AllRoutesName, 'AddToListScreen'>;
type Props = {
    navigation: ScreenNavigationProp;
};

@observer
export default class SignupScreen extends React.Component<Props>{
    @observable email = ''
    @observable password = ''
    @observable isValid = false
    @observable emailError= ''
    @observable pswdError= ''

    @action 
    private handleEmail(email: string) {
        this.email = email
    }

    @action 
    private validateEmail() {
        const emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        this.emailError = emailPattern.test(this.email) ? '' : "Invalid Email";
    }

    @action 
    private handlePswd(pswd: string) {
        this.password = pswd
    }

    @action 
    private validatePswd() {
        const pswdPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
        this.pswdError = pswdPattern.test(this.password) ? '' : "Password must contain capital,small,numeric and special char.";
    }

    @action 
    private signupValidation() {
        if(this.email ===''){
            this.emailError = "Invalid Email"
        }
        if(this.password===''){
            this.pswdError = "Invalid Password"
        }
        if (this.emailError === '' && this.pswdError === '') {
            this.isValid = true;
        }
    }

    @action 
    private clearSignup() {
        this.email = ''
        this.password = ''
        this.isValid = false
        this.emailError = ''
        this.pswdError = ''
    }

    private signUp = async () => {
        this.validateEmail();
        this.validatePswd();
        this.signupValidation();
        if (this.isValid) {
            try {
                auth()
                    .createUserWithEmailAndPassword(this.email, this.password)
                    .then(() => {
                        console.log('User account created & signed in!');
                    })
                    .catch(error => {
                        if (error.code === 'auth/email-already-in-use') {
                            this.emailError='This email address is already in use!';
                        }

                        if (error.code === 'auth/invalid-email') {
                            this.emailError ='This email address is invalid!';
                        }

                        console.error(error);
                    });
            }
            catch (err) {
                console.log('error signing up: ')
            }
        }

    }
    private gotoLogin = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <ScrollView style={defaultStyles.container}>
                <View style={defaultStyles.container}>
                    <View style={defaultStyles.logoView}>
                        <Image
                            style={defaultStyles.logoImage}
                            source={require('../../assets/images/todoLogo.png')}
                        />
                    </View>
                    <View style={defaultStyles.headingView}>
                        <View style={defaultStyles.action}>
                            <FontAwesome
                                name="user-o"
                                color={colors.BLACK}
                                size={20}
                            />
                            <Text style={defaultStyles.text_heading}>Email</Text>
                        </View>
                        <TextInput
                            placeholder="Your Email"
                            style={defaultStyles.textInput}
                            autoCapitalize="none"
                            keyboardType="email-address"
                            value={this.email}
                            onChangeText={this.handleEmail.bind(this)}
                        />
                        <Text style={defaultStyles.error} >{this.emailError}</Text>
                        <View style={defaultStyles.action}>
                            <FontAwesome
                                name="lock"
                                color={colors.BLACK}
                                size={20}
                            />
                            <Text style={defaultStyles.text_heading}>Password</Text>
                        </View>
                        <TextInput
                            placeholder="Your Password"
                            style={defaultStyles.textInput}
                            autoCapitalize="none"
                            secureTextEntry={true}
                            value={this.password}
                            onChangeText={this.handlePswd.bind(this)}
                        />
                        <Text style={defaultStyles.error} >{this.pswdError}</Text>
                    </View>
                    <View style={defaultStyles.buttonsView}>
                        <TouchableOpacity
                            style={defaultStyles.activeButton}
                            onPress={() => { this.signUp() }}
                        >
                            <Text style={defaultStyles.activebuttonText}>Sign Up</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={defaultStyles.buttonsView}>
                        <TouchableOpacity
                            style={defaultStyles.nonActiveButton}
                            onPress={() => { this.gotoLogin() }}
                        >
                            <Text style={defaultStyles.nonActiveButtonText}>Back</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )
    }
}
