import React, { Component, useEffect } from 'react'
import { TouchableOpacity,View,Text,FlatList, } from 'react-native'
import styles from './DashboardScreen.styles';
import defaultStyles from '../../styles/defaultStyles'
import colors from '../../consts/colors/colors'
import AntDesign from "react-native-vector-icons/AntDesign";
import { firebase } from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { StackNavigationProp } from '@react-navigation/stack';
import { AllRoutesName } from '../../routes/RouteNavigator';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { Tasks } from '../../interfaces/Tasks.interface';

type ScreenNavigationProp = StackNavigationProp<AllRoutesName, 'AddToListScreen'>;
type Props = {
    navigation: ScreenNavigationProp;
};

@observer
class DashboardScreen extends Component <Props>{
  @observable loading = true
  @observable tasks:Tasks[] = []
  
  componentDidMount() {
    const user = auth().currentUser;
    const unsubscribe = firestore()
      .collection('TASKS')
      .doc(user?.uid)
      .collection("USERS")
      .onSnapshot((querySnapshot) => {
        const tasks = querySnapshot.docs.map(documentSnapshot => {
          return {
            _id: documentSnapshot.id,
            name: '',
            selectedValue : '',
            ...documentSnapshot.data()
          };
        });
        this.tasks = tasks;
        if (this.loading) {
          this.loading=false;
        }
      });
    return () => unsubscribe();
  }
  
  private signOut = async() => {
    try{
        await firebase.auth().signOut()
    }
    catch(e){
        console.log(e)
    }
  }

  render(){
    return(
      <View style={defaultStyles.container}>
        <View style={defaultStyles.containerInner}>
          <Text style={defaultStyles.headerTitle}> Tasks </Text>
          <TouchableOpacity style={defaultStyles.headerButton}
            onPress={() => this.signOut()}>
            <AntDesign name="logout" size={30} color={colors.WHITE} />
          </TouchableOpacity>
        </View>
        <FlatList
          data={this.tasks}
          keyExtractor={item => item._id}
          renderItem={({ item }) => (
            <View style={styles.list}>
              <Text style={styles.flatlist}>{item.name}</Text>
            </View>
          )}
        />
      </View>
    )
  }
}

export default DashboardScreen