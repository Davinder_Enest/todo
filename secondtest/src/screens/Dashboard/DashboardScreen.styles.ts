import { StyleSheet } from 'react-native';
import colors from '../../consts/colors/colors'

const styles = StyleSheet.create({
  flatlist: {
    height: 60,
    justifyContent: "center",
    fontSize: 16,
    alignSelf: 'center'
  },
  list: {
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderLeftWidth:5,
    borderRadius:5,
    margin: 5,
    borderRightWidth:2,
    borderBottomColor: colors.GREY,
    paddingHorizontal: 5,
    borderLeftColor:colors.GREY,
    borderRightColor:colors.GREY
  },
})

export default styles;