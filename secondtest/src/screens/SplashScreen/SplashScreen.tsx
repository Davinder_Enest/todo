import React, { useEffect} from 'react';
import { Text, View, Image } from 'react-native';
import styles from '../SplashScreen/SplashScreen.styles';
import { firebase } from '@react-native-firebase/auth';
import { useNavigation } from '@react-navigation/native';

function SplashScreen (index:number,name:string) {
    const navigation = useNavigation();
    useEffect(()=>{
        NavigationToAuth()
    },[navigation])

    function NavigationToAuth(){
        setTimeout(function(){
            firebase.auth().onAuthStateChanged((user) => {
                if (user != null){
                    navigation.reset({
                        index:0,
                        routes : [{name : "BottomTab"}]
                    })
                }
                else{
                    navigation.reset({
                        index:0,
                        routes : [{name : "SignInScreen"}]
                    })
                }
            }) 
        },1000)
    }

    return (
        <View style={styles.container}>
                <Image style={styles.Logo} source={require('../../assets/images/todoLogo.png')} />
                <Text style={styles.logoText}>TODO</Text>
        </View>
    );
}

export default SplashScreen;