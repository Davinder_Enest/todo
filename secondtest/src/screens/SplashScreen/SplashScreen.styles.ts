import { StyleSheet } from 'react-native';
import colors from '../../consts/colors/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.WHITE,
        justifyContent:'center',
    },
    logoText:{
        fontSize:26,
        fontWeight:"bold",
        alignSelf:'center',
        color:colors.GREEN
    },
    Logo:{
        height :160,
        alignSelf:'center',
        width: 160,
        marginTop:'-10%'
    }
})

export default styles;