import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import styles from './AddToListScreen.styles';
import colors from '../../consts/colors/colors';
import firestore from '@react-native-firebase/firestore';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import auth from '@react-native-firebase/auth';
import { Picker } from '@react-native-community/picker';
import { StackNavigationProp } from '@react-navigation/stack';
import { AllRoutesName } from '../../routes/RouteNavigator';
import defaultStyles from '../../styles/defaultStyles';
import EditTask from '../../components/EditTaskModal/EditTaskModal';
import { observable } from 'mobx';
import { Tasks } from '../../interfaces/Tasks.interface';
import { observer } from 'mobx-react';

type ScreenNavigationProp = StackNavigationProp<AllRoutesName, 'AddToListScreen'>;
type Props = {
  navigation: ScreenNavigationProp;
};

@observer
class AddToListScreen extends Component<Props>  {
  @observable loading :boolean = true
  @observable tasks:Tasks[] = []
  @observable isModalVisible : boolean =false
  @observable textinput : string = ''
  @observable editedItem :number = 0
  @observable selectedValue : string = ''
  private user = auth().currentUser;


  private setModalVisible = (bool: boolean) => {
    this.isModalVisible = bool 
  }

  private setTextinput = (text: string) => {
    this.textinput = text 
  }

  private setEditeditem = (item: any) => {
    this.editedItem = item 
  }

  private handleEditItem = (editedItem: number) => {
    const newTask = this.tasks.map(item => {
      const editeditem = editedItem
      if (item._id === editeditem.toString()) {
        firestore()
          .collection('TASKS')
          .doc(this.user?.uid)
          .collection('USERS')
          .doc(item._id)
          .update({
            name: this.textinput
          })
          .then(() => {
            console.log('task updated!');
            return item;
          });
      }
      return item;
    });
    this.tasks = newTask;
  }

  private handlePicker = (itemValue: any, itemIndex: number) => {
    const newTask = this.tasks.map(item => {
      if (item._id === itemIndex.toString()) {
        firestore()
          .collection('TASKS')
          .doc(this.user?.uid)
          .collection('USERS')
          .doc(item._id)
          .update({
            selectedValue: itemValue
          })
          .then(() => {
            console.log('status updated!');
            return item;
          });
      }
      return item;
    }
    );
    this.tasks = newTask ;
    this.selectedValue = itemValue ;
  }
 
  componentDidMount() {
    const user = auth().currentUser;
    const unsubscribe = firestore()
      .collection('TASKS')
      .doc(user?.uid)
      .collection("USERS")
      .onSnapshot((querySnapshot) => {
        const tasks = querySnapshot.docs.map(documentSnapshot => {
          return {
            _id: documentSnapshot.id,
            name: '',
            selectedValue: '',
            ...documentSnapshot.data()
          };
        });
        this.tasks = tasks ;
        if (this.loading) {
          this.loading = false;
        }
      });
    return () => unsubscribe();//Stop listening for updates when no longer required and memory leakage
  }

  private AddTask = () => {
    this.props.navigation.navigate("AddTaskModal");
  }

  render() {
    return (
      <View style={defaultStyles.container}>
        <View style={defaultStyles.containerInner}>
          <Text style={defaultStyles.headerTitle}> Created Tasks </Text>
          <TouchableOpacity
            style={defaultStyles.headerButton}
            onPress={() => this.AddTask()}>
            <MaterialIcons name="add-circle-outline" size={40} color={colors.WHITE} />
          </TouchableOpacity>
        </View>
        <FlatList
          data={this.tasks}
          keyExtractor={item => item._id}
          renderItem={({ item }) => (
            <View style={styles.list}>
              <TouchableOpacity onPress={() => {
                this.setModalVisible(true);
                this.setTextinput(item.name);
                this.setEditeditem(item._id);
              }}>
                <Text style={styles.flatlist}>{item.name}</Text>
              </TouchableOpacity>
              <View style={styles.listInner}>
                <Picker selectedValue={item.selectedValue}
                  style={styles.picker}
                  onValueChange={(itemValue, itemIndex) => this.handlePicker.bind(itemValue, itemIndex)}
                >
                  <Picker.Item label="In-Progress" value="In-Progress" />
                  <Picker.Item label="Completed" value="Completed" />
                </Picker>
                <TouchableOpacity
                  style={styles.deletebutton}
                  onPress={() => firestore()
                    .collection('TASKS')
                    .doc(this.user?.uid)
                    .collection('USERS')
                    .doc(item._id)
                    .delete()
                    .then(() => {
                      console.log('Task deleted!');
                    })}>
                  <Text style={[styles.flatlist, styles.deletebutton]}>DELETE</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        />
        <EditTask visible={this.isModalVisible}
          defaultValue={this.textinput}
          onRequestClose={() => this.setModalVisible(false)}
          onPress={() => {
            this.handleEditItem(this.editedItem); this.setModalVisible(false);
          }}
          onChange={(text) => this.textinput= text}
        />
      </View>
    );
  }
}

export default AddToListScreen;