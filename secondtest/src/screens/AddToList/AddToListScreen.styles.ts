import { StyleSheet } from 'react-native';
import colors from '../../consts/colors/colors';

const styles = StyleSheet.create({
  Container:{
    flex:1,
    justifyContent:"center",
  },
  listDescription: {
    fontSize: 14,
    marginLeft: 15
  },
  flatlist: {
    height: 60,
    justifyContent: "center",
    fontSize: 16,
    alignSelf: 'center',
    width:'100%'
  },
  listTitle: {
    fontSize: 18,
    marginLeft: 15
  },
  list: {
    margin : 5,
    borderBottomColor: colors.GREY,
    borderTopWidth:2,
    borderTopColor:colors.GREY,
    borderBottomWidth: 2,
    borderLeftWidth:5,
    borderRadius:5,
    borderLeftColor:colors.GREY,
    borderRightColor:colors.GREY,
    borderRightWidth:2,
  },
  EditTaskTitle: {
    fontSize: 18,
    marginBottom:'5%'
  },
  listInner:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 0,
  },
  picker:{
    width:"50%",
    height: 40,
    backgroundColor: colors.GREY,
  },
  deletebutton: {
    backgroundColor: "#B22222",
    color: colors.WHITE,
    width: "50%",
    height: 40,
    textAlignVertical:'center',
    textAlign:'center'
  },
  saveButton: {
    width: '95%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.GREEN,
    borderRadius: 3,
    marginTop:'5%'
  },
  modalContainer: {
    alignItems: 'center',
  },
});

export default styles;