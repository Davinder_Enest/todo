const colors = {
    STATUSBAR : 'light-content',
    WHITE: '#FFFFFF',
    GREY :"#C8C8C8",
    TEXTINPUT : "#F5F5F5",
    PLACEHOLDER :'#989898',
    SKYBLUE : '#007FFF',
    LIGHTGREY :'#989898',
    DARKGREY : '#383838',
    BLACK : '#000',
    GREEN: '#6ce679',
    RED : 'red'
}
export default colors ;