import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Entypo from "react-native-vector-icons/Entypo";
import Feather from "react-native-vector-icons/Feather";
import { AllRoutesName } from './RouteNavigator';
import colors from '../consts/colors/colors';
import AddToListScreen from '../screens/AddToList/AddToListScreen';
import DashboardScreen from '../screens/Dashboard/DashboardScreen';

const Tab = createBottomTabNavigator<AllRoutesName>();

export function BottomTab() {
  return (
    <Tab.Navigator
      initialRouteName="AddToListScreen"
      tabBarOptions={{
        activeTintColor: colors.GREEN,
        inactiveTintColor: 'gray',
        showLabel: false,
      }}
    >
      <Tab.Screen
        name="AddToListScreen"
        component={AddToListScreen}
        options={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => (
            <Entypo name="add-to-list" color={color} size={size} />
          ),
        })}
      />
      <Tab.Screen
        name="DashboardScreen"
        component={DashboardScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Entypo name="home" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}




