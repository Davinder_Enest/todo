import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SignupScreen from '../screens/SignupScreen/SignupScreen';
import LoginScreen from '../screens/LoginScreen/LoginScreen';
import { BottomTab } from './BottomTab';
import SplashScreen from '../screens/SplashScreen/SplashScreen';
import AddTaskModal from '../components/AddTaskModal/AddTaskModal';

export type AllRoutesName = {
    SignInScreen :undefined,
    SignupScreen:undefined,
    DashboardScreen:undefined,
    AddToListScreen:undefined,
    BottomTab:undefined,
    SplashScreen:undefined,
    AddTaskModal:undefined,
    LaunchRoutes:undefined,
    EditTask : undefined
}

const MainStack = createStackNavigator<AllRoutesName>();
const RootStack = createStackNavigator<AllRoutesName>();

export function RootRoutes(props: any) {
    return (
      <RootStack.Navigator
        initialRouteName={props.initialRoute}
        screenOptions={{ headerShown: false }}>
        <RootStack.Screen name="LaunchRoutes" component={RootNavigation} />
        </RootStack.Navigator>
    )
}
export function RootNavigation() {
  return (
    <MainStack.Navigator
        initialRouteName="SplashScreen" screenOptions={{headerShown:false}}
    >
        <MainStack.Screen name="SplashScreen" component={SplashScreen} />
        <MainStack.Screen name="SignInScreen" component={LoginScreen}/>
        <MainStack.Screen name="SignupScreen" component={SignupScreen} />
        <MainStack.Screen name="BottomTab" component={BottomTab}/>
        <MainStack.Screen name="AddTaskModal" component={AddTaskModal}/>
    </MainStack.Navigator>
  );
}

