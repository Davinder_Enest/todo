import React, { Component } from 'react';
import { View, Text, TextInput, Alert, TouchableOpacity } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import Fontisto from "react-native-vector-icons/Fontisto";
import { NavigationParams, NavigationScreenProp, NavigationState, } from 'react-navigation';
import styles from './AddTaskModal.styles';
import colors from '../../consts/colors/colors';
import auth from '@react-native-firebase/auth';

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
interface State {
  taskName: string,
}

export default class AddTaskModal extends Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      taskName: ''
    }
  }
  private handleButtonPress = () => {
    const user = auth().currentUser;
    try {
      if (this.state.taskName.length > 0) {
        firestore()
          .collection('TASKS')
          .doc(user?.uid)
          .collection("USERS")
          .add({
            name: this.state.taskName,
          }
          )
          .then(docRef => {
            this.props.navigation.navigate('AddToListScreen');
          });
      }
    }
    catch (error) {
      Alert.alert(error.message)
      console.log(error)
    }
  }
  private onPressClose = () => {
    this.props.navigation.goBack();
  }
  render() {
    return (
      <View style={styles.rootContainer}>
        <View style={styles.closeButtonContainer}>
          <TouchableOpacity
            onPress={() => this.onPressClose()}>
            <Fontisto name="close" size={25} color={colors.DARKGREY} />
          </TouchableOpacity>
        </View>
        <View style={styles.innerContainer}>
          <Text style={styles.title}>Write your task</Text>
          <TextInput
            style={styles.input}
            placeholder='Task'
            value={this.state.taskName}
            onChangeText={(text) => this.setState({ taskName: text })}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.handleButtonPress()}
            disabled={this.state.taskName.length === 0}
          >
            <Text style={styles.buttonLabel}>Add</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

