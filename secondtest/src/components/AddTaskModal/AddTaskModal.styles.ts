import { StyleSheet } from 'react-native';
import colors from '../../consts/colors/colors';

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    backgroundColor: colors.WHITE
  },
  closeButtonContainer: {
    position: 'absolute',
    top: 10,
    right: 10,
    zIndex: 1,
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  button: {
    width: '90%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: colors.GREEN,
    alignSelf: 'center',
    margin: 5,
  },
  title: {
    fontSize: 20,
    marginBottom: 10,
    marginLeft: 6,
    alignSelf: 'center'
  },
  buttonLabel: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.WHITE,
  },
  input: {
    width: '90%',
    borderColor: colors.GREY,
    backgroundColor: colors.WHITE,
    borderWidth: 1,
    margin: 5,
    height: 40,
    borderRadius: 3,
    color: colors.DARKGREY,
    alignSelf: 'center'
  },
});

export default styles