import React, { Component } from 'react';
import { View, Text,TouchableOpacity, Modal, TextInput } from 'react-native';
import styles from '../../screens/AddToList/AddToListScreen.styles';
import defaultStyles from '../../styles/defaultStyles';
import { observable } from 'mobx';
import { Tasks } from '../../interfaces/Tasks.interface';

type Props = {
    visible:boolean;
    defaultValue:string;
    onRequestClose: ()=>void;
    onPress:()=>void
    onChange:(text:string)=>void;
};

class EditTask extends Component<Props>  {
  
  @observable loading :boolean = true
  @observable tasks:Tasks[] = []
  @observable isModalVisible : boolean =false
  @observable textinput : string = ''
  @observable editedItem :number = 0
  @observable selectedValue : string = ''
  
  render() {
    const{
        visible,
        defaultValue,
        onRequestClose,
        onPress,
        onChange
      } = this.props;
    return (
      <View style={styles.Container}>
        <Modal animationType="fade" visible={visible}
          onRequestClose={onRequestClose}
        >
          <View style={[styles.Container,styles.modalContainer]}>
            <Text style={styles.EditTaskTitle}>Change task:</Text>
            <TextInput style={defaultStyles.textInput} editable={true}
              defaultValue={defaultValue}
              onChangeText={onChange}
            />
            <TouchableOpacity
              style={styles.saveButton}
              onPress={onPress}
              >
              <Text style={defaultStyles.activebuttonText}>Save</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

export default EditTask;